﻿using AutoMapper;
using ProjectStructure.BLL.DTOs;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();

            CreateMap<TeamDTO, Team>()
                .ForMember(x => x.UpdatedAt, m => m.Ignore())
                .ForMember(x => x.CreatedAt, m => m.Ignore());
        }
    }
}
