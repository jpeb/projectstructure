﻿using AutoMapper;
using ProjectStructure.BLL.DTOs;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(x => x.RegisteredAt, m => m.MapFrom(p => p.CreatedAt));

            CreateMap<UserDTO, User>()
                .ForMember(x => x.UpdatedAt, m => m.Ignore())
                .ForMember(x => x.CreatedAt, m => m.Ignore());
        }
    }
}
