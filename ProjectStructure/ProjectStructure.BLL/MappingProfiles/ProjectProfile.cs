﻿using AutoMapper;
using ProjectStructure.BLL.DTOs.Project;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<ProjectDTO, Project>()
                .ForMember(x => x.UpdatedAt, m => m.Ignore())
                .ForMember(x => x.CreatedAt, m => m.Ignore());
        }
    }
}
