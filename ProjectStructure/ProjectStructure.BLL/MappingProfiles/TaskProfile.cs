﻿using AutoMapper;
using ProjectStructure.BLL.DTOs.Task;
using Task = ProjectStructure.DAL.Entities.Task;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<Task, TaskNameDTO>();

            CreateMap<TaskDTO, Task>()
                .ForMember(x => x.UpdatedAt, m => m.Ignore())
                .ForMember(x => x.CreatedAt, m => m.Ignore());
        }
    }
}
