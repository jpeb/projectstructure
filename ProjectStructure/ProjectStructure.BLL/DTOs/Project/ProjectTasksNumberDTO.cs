﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.DTOs.Project
{
    public class ProjectTasksNumberDTO
    {
        public ProjectDTO Project { get; set; }
        public int TasksNumber { get; set; }
    }
}
