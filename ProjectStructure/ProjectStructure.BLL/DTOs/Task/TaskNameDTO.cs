﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.DTOs.Task
{
    public class TaskNameDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
