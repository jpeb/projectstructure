﻿using AutoMapper;
using ProjectStructure.BLL.DTOs;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services
{
    public class TeamsService : BaseService, ITeamsService
    {
        private readonly IRepository<Team> _repository;

        public TeamsService(IMapper mapper, IRepository<Team> repository) : base(mapper)
        {
            _repository = repository;
        }

        public IEnumerable<TeamDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(_repository.GetAll());
        }

        public TeamDTO Get(int teamId)
        {
            return _mapper.Map<TeamDTO>(_repository.GetById(teamId));
        }

        public TeamDTO Add(TeamDTO team)
        {
            var newTeam = _repository.Add(_mapper.Map<Team>(team));

            return _mapper.Map<TeamDTO>(newTeam);
        }

        public void Delete(int teamId)
        {
            _repository.Delete(_repository.GetById(teamId));
        }

        public void Update(TeamDTO newTeam)
        {
            var team = _repository.GetById(newTeam.Id);

            if (team is null)
                throw new ArgumentException($"Team with id={newTeam.Id} not found");

            _mapper.Map(newTeam, team);
            team.UpdatedAt = DateTime.Now;

            _repository.Update(team);
        }
    }
}
