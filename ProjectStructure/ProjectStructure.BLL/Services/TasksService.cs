﻿using AutoMapper;
using ProjectStructure.BLL.DTOs.Task;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = ProjectStructure.DAL.Entities.Task;

namespace ProjectStructure.BLL.Services
{
    public class TasksService : BaseService, ITasksService
    {
        private readonly IRepository<Task> _repository;

        public TasksService(IMapper mapper, IRepository<Task> repository) : base(mapper)
        {
            _repository = repository;
        }

        public IEnumerable<TaskDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_repository.GetAll());
        }

        public TaskDTO Get(int taskId)
        {
            return _mapper.Map<TaskDTO>(_repository.GetById(taskId));
        }

        public TaskDTO Add(TaskDTO task)
        {
            var newTask = _repository.Add(_mapper.Map<Task>(task));

            return _mapper.Map<TaskDTO>(newTask);
        }

        public void Delete(int taskId)
        {
            _repository.Delete(_repository.GetById(taskId));
        }

        public void Update(TaskDTO newTask)
        {
            var task = _repository.GetById(newTask.Id);

            if (task is null)
                throw new ArgumentException($"Task with id={newTask.Id} not found");

            _mapper.Map(newTask, task);
            task.UpdatedAt = DateTime.Now;

            _repository.Update(task);
        }

        // Query 2
        public ICollection<TaskDTO> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength)
        {
            var tasks = _repository
                    .GetAll(t => t.PerformerId == userId && t.Name.Length < maxTaskNameLength);

            return _mapper.Map<ICollection<TaskDTO>>(tasks);
        }

        // Query 3
        public ICollection<TaskNameDTO> GetUserTasksFinishedInYear(int userId, int finishedYear)
        {
            var tasks = _repository
                .GetAll(t => t.PerformerId == userId &&
                                t.FinishedAt.HasValue &&
                                t.FinishedAt.Value.Year == finishedYear);

            return _mapper.Map<ICollection<TaskNameDTO>>(tasks);
        }
    }
}
