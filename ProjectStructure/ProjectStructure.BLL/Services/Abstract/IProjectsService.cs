﻿using ProjectStructure.BLL.DTOs.Project;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services.Abstract
{
    public interface IProjectsService
    {
        IEnumerable<ProjectDTO> GetAll();
        ProjectDTO Get(int projectId);
        void Update(ProjectDTO project);
        ProjectDTO Add(ProjectDTO project);
        void Delete(int projectId);
        ICollection<ProjectTasksNumberDTO> GetUserProjectsTasksNumber(int userId);
    }
}
