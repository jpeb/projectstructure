﻿using ProjectStructure.BLL.DTOs.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services.Abstract
{
    public interface ITasksService
    {
        IEnumerable<TaskDTO> GetAll();
        TaskDTO Get(int taskId);
        void Update(TaskDTO task);
        TaskDTO Add(TaskDTO task);
        void Delete(int taskId);
        ICollection<TaskDTO> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength);
        ICollection<TaskNameDTO> GetUserTasksFinishedInYear(int userId, int finishedYear);
    }
}
