﻿using ProjectStructure.BLL.DTOs;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services.Abstract
{
    public interface IUsersService
    {
        IEnumerable<UserDTO> GetAll();
        UserDTO Get(int userId);
        void Update(UserDTO user);
        UserDTO Add(UserDTO user);
        void Delete(int userId);
    }
}
