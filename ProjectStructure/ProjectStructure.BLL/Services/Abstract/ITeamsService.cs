﻿using ProjectStructure.BLL.DTOs;
using System;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services.Abstract
{
    public interface ITeamsService
    {
        IEnumerable<TeamDTO> GetAll();
        TeamDTO Get(int teamId);
        void Update(TeamDTO team);
        TeamDTO Add(TeamDTO team);
        void Delete(int teamId);
    }
}
