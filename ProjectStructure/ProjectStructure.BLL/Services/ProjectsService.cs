﻿using AutoMapper;
using ProjectStructure.BLL.DTOs.Project;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class ProjectsService : BaseService, IProjectsService
    {
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Task> _tasksRepository;

        public ProjectsService(IMapper mapper, IRepository<Project> projectRepository, IRepository<Task> tasksRepository) : base(mapper)
        {
            _projectRepository = projectRepository;
            _tasksRepository = tasksRepository;
        }

        public IEnumerable<ProjectDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<ProjectDTO>>(_projectRepository.GetAll());
        }

        public ProjectDTO Get(int projectId)
        {
            return _mapper.Map<ProjectDTO>(_projectRepository.GetById(projectId));
        }

        public ProjectDTO Add(ProjectDTO project)
        {
            var newProject = _projectRepository.Add(_mapper.Map<Project>(project));

            return _mapper.Map<ProjectDTO>(newProject);
        }

        public void Delete(int projectId)
        {
            _projectRepository.Delete(_projectRepository.GetById(projectId));
        }

        public void Update(ProjectDTO newProject)
        {
            var project = _projectRepository.GetById(newProject.Id);

            if (project is null)
                throw new ArgumentException($"Project with id={newProject.Id} not found");

            _mapper.Map(newProject, project);
            project.UpdatedAt = DateTime.Now;

            _projectRepository.Update(project);
        }

        // Query 1
        public ICollection<ProjectTasksNumberDTO> GetUserProjectsTasksNumber(int userId)
        {
            return _projectRepository.GetAll(p => p.AuthorId == userId)
                .GroupJoin(_tasksRepository.GetAll(), p => p.Id, t => t.ProjectId,
                    (project, tasks) => new ProjectTasksNumberDTO { Project = _mapper.Map<ProjectDTO>(project), TasksNumber = tasks.Count() })
                .ToList();
        }
    }
}
