﻿using AutoMapper;
using ProjectStructure.BLL.DTOs;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services
{
    public class UsersService : BaseService, IUsersService
    {
        private readonly IRepository<User> _repository;

        public UsersService(IMapper mapper, IRepository<User> repository) : base(mapper)
        {
            _repository = repository;
        }

        public IEnumerable<UserDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_repository.GetAll());
        }

        public UserDTO Get(int userId)
        {
            return _mapper.Map<UserDTO>(_repository.GetById(userId));
        }

        public UserDTO Add(UserDTO user)
        {
            var newUser = _repository.Add(_mapper.Map<User>(user));

            return _mapper.Map<UserDTO>(newUser);
        }

        public void Delete(int userId)
        {
            _repository.Delete(_repository.GetById(userId));
        }

        public void Update(UserDTO newUser)
        {
            var user = _repository.GetById(newUser.Id);

            if (user is null)
                throw new ArgumentException($"User with id={newUser.Id} not found");

            _mapper.Map(newUser, user);
            user.UpdatedAt = DateTime.Now;

            _repository.Update(user);
        }
    }
}
