﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTOs.Task;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;
using System;
using System.Linq;

namespace ProjectStructure.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService _tasksService;

        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public ActionResult<ICollection<TaskDTO>> GetAll()
        {
            return _tasksService.GetAll().ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            var task = _tasksService.Get(id);

            if (task is null) return NotFound();

            return task;
        }

        [HttpGet("assignedToUser/{userId}")]
        public ActionResult<ICollection<TaskDTO>> GetUserTasksWithNameLessThen(int userId, [FromQuery]int? maxTaskNameLength)
        {
            maxTaskNameLength ??= int.MaxValue;

            return _tasksService.GetUserTasksWithNameLessThen(userId, maxTaskNameLength.Value).ToList();
        }

        [HttpGet("finishedInYear/{finishedYear}")]
        public ActionResult<ICollection<TaskNameDTO>> GetUserTasksFinishedInYear(int finishedYear, [FromQuery] int userId)
        {
            return _tasksService.GetUserTasksFinishedInYear(userId, finishedYear).ToList();
        }

        [HttpPost]
        public ActionResult Add([FromBody] TaskDTO newTask)
        {
            var createdTask = _tasksService.Add(newTask);

            return Created($"tasks/{createdTask.Id}", createdTask);
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, [FromBody] TaskDTO newTask)
        {
            newTask.Id = id;

            try
            {
                _tasksService.Update(newTask);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _tasksService.Delete(id);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
