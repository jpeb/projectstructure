﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTOs;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;
using System;
using System.Linq;

namespace ProjectStructure.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;

        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpGet]
        public ActionResult<ICollection<UserDTO>> GetAll()
        {
            return _usersService.GetAll().ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            var user = _usersService.Get(id);

            if (user is null) return NotFound();

            return user;
        }

        [HttpPost]
        public ActionResult Add([FromBody] UserDTO newUser)
        {
            var createdUser = _usersService.Add(newUser);

            return Created($"users/{createdUser.Id}", createdUser);
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, [FromBody] UserDTO newUser)
        {
            newUser.Id = id;

            try
            {
                _usersService.Update(newUser);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _usersService.Delete(id);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
