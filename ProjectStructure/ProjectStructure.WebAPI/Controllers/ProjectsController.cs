﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTOs.Project;
using ProjectStructure.BLL.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;

        public ProjectsController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        [HttpGet]
        public ActionResult<ICollection<ProjectDTO>> GetAll()
        {
            return _projectsService.GetAll().ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            var project = _projectsService.Get(id);

            if (project is null) return NotFound();

            return project;
        }

        [HttpGet("tasksNumber/{userId}")]
        public ActionResult<ICollection<ProjectTasksNumberDTO>> GetUserProjectsTasksNumber(int userId)
        {
            return _projectsService.GetUserProjectsTasksNumber(userId).ToList();
        }

        [HttpPost]
        public ActionResult Add([FromBody] ProjectDTO newProject)
        {
            var createdProject = _projectsService.Add(newProject);

            return Created($"projects/{createdProject.Id}", createdProject);
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, [FromBody] ProjectDTO newProject)
        {
            newProject.Id = id;

            try
            {
                _projectsService.Update(newProject);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _projectsService.Delete(id);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
