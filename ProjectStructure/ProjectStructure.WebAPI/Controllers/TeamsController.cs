﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTOs;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;
using System;
using System.Linq;

namespace ProjectStructure.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService _teamsService;

        public TeamsController(ITeamsService teamsService)
        {
            _teamsService = teamsService;
        }

        [HttpGet]
        public ActionResult<ICollection<TeamDTO>> GetAll()
        {
            return _teamsService.GetAll().ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            var team = _teamsService.Get(id);

            if (team is null) return NotFound();

            return team;
        }

        [HttpPost]
        public ActionResult Add([FromBody] TeamDTO newTeam)
        {
            var createdTeam = _teamsService.Add(newTeam);

            return Created($"teams/{createdTeam.Id}", createdTeam);
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, [FromBody] TeamDTO newTeam)
        {
            newTeam.Id = id;

            try
            {
                _teamsService.Update(newTeam);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _teamsService.Delete(id);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
