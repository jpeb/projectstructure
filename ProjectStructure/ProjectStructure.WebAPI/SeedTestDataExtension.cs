﻿using Bogus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;

namespace ProjectStructure.WebAPI
{
    public static class SeedTestDataExtension
    {
        // todo: move to data access layer
        public static IHost SeedTestData(this IHost host, int teamsNumber, int usersNumber, int projectsNumber, int tasksNumber)
        {

            var teamsRepository = host.Services.GetRequiredService<IRepository<Team>>();
            var usersRepository = host.Services.GetRequiredService<IRepository<User>>();
            var projectsRepository = host.Services.GetRequiredService<IRepository<Project>>();
            var tasksRepository = host.Services.GetRequiredService<IRepository<Task>>();


            var teams = GenerateRandomTeams(teamsNumber);
            teamsRepository.AddRange(teams);

            var users = GenerateRandomUsers(usersNumber, teamsRepository.GetAll());
            usersRepository.AddRange(users);

            var projects = GenerateRandomProjects(projectsNumber, teamsRepository.GetAll(), usersRepository.GetAll());
            projectsRepository.AddRange(projects);

            var tasks = GenerateRandomTasks(tasksNumber, projectsRepository.GetAll(), usersRepository.GetAll());
            tasksRepository.AddRange(tasks);

            return host;
        }

        public static ICollection<Team> GenerateRandomTeams(int number)
        {
            var teamsFake = new Faker<Team>()
               .RuleFor(p => p.Id, f => 0)
               .RuleFor(pi => pi.CreatedAt, f => DateTime.Now)
               .RuleFor(pi => pi.UpdatedAt, f => DateTime.Now)
               .RuleFor(pi => pi.Name, f => f.Random.Words(f.Random.Number(1, 5)))
               .StrictMode(true);

            return teamsFake.Generate(number);
        }

        public static ICollection<User> GenerateRandomUsers(int number, IEnumerable<Team> teams)
        {
            var uersFake = new Faker<User>()
               .RuleFor(p => p.Id, f => 0)
               .RuleFor(pi => pi.CreatedAt, f => DateTime.Now)
               .RuleFor(pi => pi.UpdatedAt, f => DateTime.Now)
               .RuleFor(pi => pi.TeamId, f => f.PickRandom(teams).Id)
               .RuleFor(pi => pi.FirstName, f => f.Name.FirstName())
               .RuleFor(pi => pi.LastName, f => f.Name.LastName())
               .RuleFor(pi => pi.Email, f => f.Internet.Email())
               .RuleFor(pi => pi.BirthDay, f => f.Date.Past(f.Random.Number(25, 50)))
               .StrictMode(true);

            return uersFake.Generate(number);
        }

        public static ICollection<Project> GenerateRandomProjects(int number, IEnumerable<Team> teams, IEnumerable<User> users)
        {
            var projectFake = new Faker<Project>()
               .RuleFor(p => p.Id, f => 0)
               .RuleFor(pi => pi.CreatedAt, f => DateTime.Now)
               .RuleFor(pi => pi.UpdatedAt, f => DateTime.Now)
               .RuleFor(pi => pi.AuthorId, f => f.PickRandom(users).Id)
               .RuleFor(pi => pi.TeamId, f => f.PickRandom(teams).Id)
               .RuleFor(pi => pi.Name, f => f.Random.Words(f.Random.Number(2, 5)))
               .RuleFor(pi => pi.Description, f => f.Lorem.Sentences(f.Random.Number(1, 3)))
               .RuleFor(pi => pi.Deadline, f => f.Date.Future(f.Random.Number(1, 3)))
               .StrictMode(true);

            return projectFake.Generate(number);
        }

        public static ICollection<Task> GenerateRandomTasks(int number, IEnumerable<Project> projects, IEnumerable<User> users)
        {
            TaskState taskState = TaskState.Created;
            var taskFake = new Faker<Task>()
               .RuleFor(p => p.Id, f => 0)
               .RuleFor(pi => pi.CreatedAt, f => DateTime.Now)
               .RuleFor(pi => pi.UpdatedAt, f => DateTime.Now)
               .RuleFor(pi => pi.ProjectId, f => f.PickRandom(projects).Id)
               .RuleFor(pi => pi.PerformerId, f => f.PickRandom(users).Id)
               .RuleFor(pi => pi.Name, f => f.Random.Words(f.Random.Number(1, 3)))
               .RuleFor(pi => pi.Description, f => f.Lorem.Sentences(f.Random.Number(1, 2)))
               .RuleFor(pi => pi.Name, f => f.Random.Words(f.Random.Number(1, 5)))
               .RuleFor(pi => pi.Description, f => f.Lorem.Sentences(f.Random.Number(1, 3)))
               .RuleFor(pi => pi.State, f => { taskState = f.Random.Enum<TaskState>(); return taskState; })
               .RuleFor(pi => pi.FinishedAt, f => taskState == TaskState.Finished ? f.Date.Past() : null)
               .StrictMode(true);

            return taskFake.Generate(number);
        }
    }
}
