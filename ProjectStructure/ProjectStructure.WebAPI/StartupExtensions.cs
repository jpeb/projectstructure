﻿using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories;
using ProjectStructure.DAL.Repositories.Interfaces;
using System.Reflection;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI
{
    public static class StartupExtensions
    {
        public static void RegisterRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IRepository<Project>, InMemoryRepository<Project>>();
            services.AddSingleton<IRepository<Task>, InMemoryRepository<Task>>();
            services.AddSingleton<IRepository<User>, InMemoryRepository<User>>();
            services.AddSingleton<IRepository<Team>, InMemoryRepository<Team>>();
        }

        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IProjectsService, ProjectsService>();
            services.AddScoped<ITasksService, TasksService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<ITeamsService, TeamsService>();
        }

        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
