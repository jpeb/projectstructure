using ProjectStructure.DAL.Entities.Abstract;
using System;

namespace ProjectStructure.DAL.Entities
{
    public class User : BaseEntity
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
