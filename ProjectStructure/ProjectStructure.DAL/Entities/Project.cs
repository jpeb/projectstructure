using ProjectStructure.DAL.Entities.Abstract;
using System;

namespace ProjectStructure.DAL.Entities
{
    public class Project : BaseEntity
    {
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
    }
}
