﻿using ProjectStructure.DAL.Entities.Abstract;
using ProjectStructure.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly List<T> _entities;

        public InMemoryRepository()
        {
            _entities = new List<T>();
        }

        public InMemoryRepository(List<T> entites)
        {
            _entities = entites;
        }

        public T GetById(int id)
        {
            return _entities.SingleOrDefault(e => e.Id == id);
        }

        public IEnumerable<T> GetAll(Func<T, bool> filter = null)
        {
            return filter is not null 
                ? _entities.Where(filter).ToList() 
                : _entities.ToList();
        }

        public T Add(T entity)
        {
            if(entity is null)
                throw new ArgumentNullException(nameof(entity));

            var id = !_entities.Any() ? 0 : _entities.Max(x => x.Id) + 1;
            entity.Id = id;

            _entities.Add(entity);

            return entity;
        }

        public void AddRange(IEnumerable<T> entities)
        {
            if (entities is null)
                throw new ArgumentNullException(nameof(entities));

            var id = !_entities.Any() ? 0 : _entities.Max(x => x.Id) + 1;

            _entities.AddRange(entities.Select(e => { e.Id = id++; return e; }));
        }

        public void Update(T entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            var entityIndex = _entities.FindIndex(e => e.Id == entity.Id);

            if (entityIndex < 0)
                throw new ArgumentException("Entity not found", nameof(entity));

            _entities[entityIndex] = entity;
        }

        public void Delete(T entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            _entities.Remove(entity);
        }
    }
}
